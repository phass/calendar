import pytest

from app.manager.user_manager import UserManager


@pytest.mark.incremental
class TestUserManager:
    um = UserManager()

    NAME = "test_name"
    TIME_ZONE = "Poland"
    GUEST_EVENT_EDIT_DATA = {}

    USERS_TO_ADD = 5

    def test_0_add(self):
        for i in range(self.USERS_TO_ADD):
            self.um.add(name=self.NAME * i, time_zone=self.TIME_ZONE, guest_event_edit_data=self.GUEST_EVENT_EDIT_DATA)
        assert len(self.um.get_list()) is self.USERS_TO_ADD

    def test_1_get(self):
        get = self.um.get(name=self.NAME)
        assert get.name == self.NAME
        assert get.time_zone == self.TIME_ZONE
        assert get.guest_event_edit_data == self.GUEST_EVENT_EDIT_DATA

    def test_2_get_list(self):
        assert len(self.um.get_list()) is self.USERS_TO_ADD

    def test_3_get_names(self):
        names = self.um.get_names()
        assert names == [i.name for i in self.um.get_list()]

    def test_4_get_names_with_rm_param(self):
        names = self.um.get_names(rm_names=[self.NAME, self.NAME])
        names_list = [i.name for i in self.um.get_list()]
        names_list.remove(self.NAME)
        assert names == names_list

    def test_5_delete(self):
        self.um.delete(name=self.NAME)
        assert self.NAME not in self.um.get_names()
