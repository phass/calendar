import pytest

from app.manager.calendar_manager import CalendarManager
from app.manager.event_manager import EventManager
from app.manager.user_manager import UserManager
from app.model.event import EventType, RSVP


@pytest.mark.incremental
class TestCalendarManager:
    cm = CalendarManager()
    em = EventManager()
    um = UserManager()

    NAME = "test"
    COLOR = "red"
    CALENDAR_OWNER = "test_user"
    SHARE = {}

    CALENDARS_TO_ADD = 5

    USER = "test_user"

    GUEST_EVENT_EDIT_DATA = {}

    EVENT_TYPE_ALL_DAY = EventType.ALL_DAY.value
    TITLE = "title"
    DESCRIPTION = ""
    CALENDAR = "test"
    START_DATE = "2016-10-16"
    END_DATE = "2016-10-18"
    EVENT_OWNER = "test_user"
    OWNER_RSVP = RSVP.UNKNOWN.value
    GUESTS = {}

    EVENT_TYPE_NORMAL = EventType.NORMAL.value
    START_TIME = "08:00"
    END_TIME = "24:00"
    TIME_ZONE = "Europe/London"

    def test_0_add(self):
        for i in range(self.CALENDARS_TO_ADD):
            self.cm.add(name=self.NAME * i, color=self.COLOR, owner=self.CALENDAR_OWNER, share=self.SHARE)
        assert len(self.cm.get_list()) is self.CALENDARS_TO_ADD

    def test_1_get(self):
        calendar = self.cm.get(calendar_name=self.NAME)
        assert calendar.name == self.NAME
        assert calendar.color == self.COLOR
        assert calendar.owner == self.CALENDAR_OWNER
        assert calendar.share == self.SHARE

    def test_2_get_list(self):
        assert len(self.cm.get_list()) is self.CALENDARS_TO_ADD

    def test_3_get_names(self):
        names = self.cm.get_names()
        assert names == [i.name for i in self.cm.get_list()]

    def test_4_get_user_calendars(self):
        self.cm.add(name="Kazuya calendar", color="AB1871", owner="Kazuya",
                    share={'Czester': 'write', self.USER: "read"})
        self.cm.add(name="Czester calendar", color="39AB4F", owner="Czester", share=None)
        self.cm.add(name="Cassandra calendar", color="FF88A4", owner="Cassandra",
                    share={'Czester': 'write', 'Kazuya': 'read', self.USER: "write"})
        owner_calendars, shared_calendars = self.cm.get_user_calendars(user=self.USER)
        assert len(owner_calendars) is self.CALENDARS_TO_ADD
        assert len(shared_calendars) is 2

