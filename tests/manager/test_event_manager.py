import pytest

from app.manager.calendar_manager import CalendarManager
from app.manager.event_manager import EventManager
from app.manager.user_manager import UserManager
from app.model.event import EventType, RSVP, Event


@pytest.mark.incremental
class TestEventManager:
    cm = CalendarManager()
    em = EventManager()
    um = UserManager()

    EVENTS_TO_ADD = 5

    EVENT_TYPE_ALL_DAY = EventType.ALL_DAY.value
    TITLE = "title"
    DESCRIPTION = ""
    CALENDAR = "test"
    START_DATE = "2016-10-16"
    END_DATE = "2016-10-18"
    OWNER = "test_user"
    OWNER_RSVP = RSVP.UNKNOWN.value
    GUESTS = {}

    EVENT_TYPE_NORMAL = EventType.NORMAL.value
    START_TIME = "08:00"
    END_TIME = "00:00"
    TIME_ZONE = "Europe/London"

    USER_TIMEZONE = "Asia/Tokyo"

    def test_0_add(self):
        for i in range(self.EVENTS_TO_ADD):
            self.em.add(event_type=self.EVENT_TYPE_NORMAL, title=self.TITLE * i, description=self.DESCRIPTION,
                        calendar=self.CALENDAR, start_date=self.START_DATE, end_date=self.END_DATE, owner=self.OWNER,
                        start_time=self.START_TIME, end_time=self.END_TIME, time_zone=self.TIME_ZONE,
                        guests=self.GUESTS,
                        owner_rsvp=self.OWNER_RSVP)

        self.em.add(event_type=self.EVENT_TYPE_NORMAL, title=self.TITLE * 10, description=self.DESCRIPTION,
                    calendar=self.CALENDAR, start_date=self.START_DATE, end_date=self.END_DATE, owner=self.OWNER * 2,
                    start_time=self.START_TIME, end_time=self.END_TIME, time_zone=self.TIME_ZONE,
                    guests={self.OWNER: 'unknown'},
                    owner_rsvp=self.OWNER_RSVP)

        assert len(self.em.get_list()) is self.EVENTS_TO_ADD + 1

    def test_1_get(self):
        event = self.em.get(title=self.TITLE)
        assert event.event_type == self.EVENT_TYPE_NORMAL
        assert event.title == self.TITLE
        assert event.description == self.DESCRIPTION
        assert event.calendar == self.CALENDAR
        assert event.start_date == self.START_DATE
        assert event.end_date == self.END_DATE
        assert event.owner == self.OWNER
        assert event.owner_rsvp == self.OWNER_RSVP
        assert event.guests == self.GUESTS
        assert event.start_time == self.START_TIME
        assert event.end_time == self.END_TIME
        assert event.time_zone == self.TIME_ZONE

    def test_2_get_list(self):
        assert len(self.em.get_list()) is self.EVENTS_TO_ADD + 1

    def test_3_get_names(self):
        names = self.em.get_names()
        assert names == [i.title for i in self.em.get_list()]

    def test_4_get_user_events(self):
        self.um.add(name=self.OWNER, time_zone=self.USER_TIMEZONE, guest_event_edit_data={})
        owner_events, guest_events = self.em.get_user_events(self.OWNER)
        assert len(owner_events) is 5
        assert len(guest_events) is 1

    def test_5_get_events_by_date(self):
        get_events = self.em.get_events_by_date(day="2016-10-16")
        assert len(get_events) is 6
        get_events = self.em.get_events_by_date(start_week="2016-10-16", end_week="2016-10-24")
        assert len(get_events) is 6
        get_events = self.em.get_events_by_date(month="2016-10")
        assert len(get_events) is 6
        get_events = self.em.get_events_by_date(year="2016")
        assert len(get_events) is 6

    def test_6_adjust_event_to_user_time_zone(self):
        event_before = Event(event_type=self.EVENT_TYPE_NORMAL, title=self.TITLE, description=self.DESCRIPTION,
                             calendar=self.CALENDAR, start_date=self.START_DATE, end_date=self.END_DATE,
                             owner=self.OWNER,
                             start_time=self.START_TIME, end_time=self.END_TIME, time_zone=self.TIME_ZONE,
                             guests=self.GUESTS,
                             owner_rsvp=self.OWNER_RSVP)
        event_after = self.em.adjust_event_to_user_time_zone(event_before, self.em, tz_to=self.USER_TIMEZONE)
        assert event_after.event_type == self.EVENT_TYPE_NORMAL
        assert event_after.title == self.TITLE
        assert event_after.description == self.DESCRIPTION
        assert event_after.calendar == self.CALENDAR
        assert event_after.start_date == self.START_DATE
        assert event_after.end_date == self.END_DATE
        assert event_after.owner == self.OWNER
        assert event_after.owner_rsvp == self.OWNER_RSVP
        assert event_after.guests == self.GUESTS
        assert event_after.start_time == "16:00"
        assert event_after.end_time == "08:00"
        assert event_after.time_zone == self.TIME_ZONE

    def test_7_convert_event_date_and_time_to_user_time_zone(self):
        start_date, start_time, end_date, end_time, tz_from, tz_to = \
            ("2016-10-18", "08:00", "2016-10-18", "16:00", "Poland", "Asia/Qatar")
        start_date_converted, start_time_converted, end_date_converted, end_time_converted = \
            self.em.convert_event_date_and_time_to_user_time_zone(start_date, start_time, end_date, end_time, tz_from,
                                                                  tz_to)
        assert start_date_converted == start_date
        assert start_time_converted == "09:00"
        assert end_date_converted == end_date
        assert end_time_converted == "17:00"

