from app.model.calendar import Calendar
from app.model.event import Event, EventType, RSVP
from app.model.user import User


class TestUser:
    NAME = "test_name"
    TIME_ZONE = "Poland"
    GUEST_EVENT_EDIT_DATA = {}

    def test_user(self):
        user = User(name=self.NAME, time_zone=self.TIME_ZONE, guest_event_edit_data=self.GUEST_EVENT_EDIT_DATA)
        assert user.name == self.NAME
        assert user.time_zone == self.TIME_ZONE
        assert user.guest_event_edit_data == self.GUEST_EVENT_EDIT_DATA


class TestCalendar:
    NAME = "test"
    COLOR = "red"
    OWNER = "test_user"
    SHARE = {}

    def test_calendar(self):
        calendar = Calendar(name=self.NAME, color=self.COLOR, owner=self.OWNER, share=self.SHARE)
        assert calendar.name == self.NAME
        assert calendar.color == self.COLOR
        assert calendar.owner == self.OWNER
        assert calendar.share == self.SHARE


class TestEvent:
    EVENT_TYPE_ALL_DAY = EventType.ALL_DAY.value
    TITLE = "title"
    DESCRIPTION = ""
    CALENDAR = "test"
    START_DATE = "2016-10-16"
    END_DATE = "2016-10-18"
    OWNER = "test_user"
    OWNER_RSVP = RSVP.UNKNOWN.value
    GUESTS = {}

    EVENT_TYPE_NORMAL = EventType.NORMAL.value
    START_TIME = "08:00"
    END_TIME = "24:00"
    TIME_ZONE = "Europe/London"

    def test_all_day_event(self):
        event = Event(event_type=self.EVENT_TYPE_ALL_DAY, title=self.TITLE, description=self.DESCRIPTION,
                      calendar=self.CALENDAR, start_date=self.START_DATE, end_date=self.END_DATE, owner=self.OWNER,
                      guests=self.GUESTS, owner_rsvp=self.OWNER_RSVP, start_time=None, end_time=None, time_zone=None)
        assert event.event_type == self.EVENT_TYPE_ALL_DAY
        assert event.title == self.TITLE
        assert event.description == self.DESCRIPTION
        assert event.calendar == self.CALENDAR
        assert event.start_date == self.START_DATE
        assert event.end_date == self.END_DATE
        assert event.owner == self.OWNER
        assert event.owner_rsvp == self.OWNER_RSVP
        assert event.guests == self.GUESTS

    def test_normal_event(self):
        event = Event(event_type=self.EVENT_TYPE_NORMAL, title=self.TITLE, description=self.DESCRIPTION,
                      calendar=self.CALENDAR, start_date=self.START_DATE, end_date=self.END_DATE, owner=self.OWNER,
                      start_time=self.START_TIME, end_time=self.END_TIME, time_zone=self.TIME_ZONE, guests=self.GUESTS,
                      owner_rsvp=self.OWNER_RSVP)
        assert event.event_type == self.EVENT_TYPE_NORMAL
        assert event.title == self.TITLE
        assert event.description == self.DESCRIPTION
        assert event.calendar == self.CALENDAR
        assert event.start_date == self.START_DATE
        assert event.end_date == self.END_DATE
        assert event.owner == self.OWNER
        assert event.owner_rsvp == self.OWNER_RSVP
        assert event.guests == self.GUESTS
        assert event.start_time == self.START_TIME
        assert event.end_time == self.END_TIME
        assert event.time_zone == self.TIME_ZONE
