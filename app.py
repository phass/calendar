import calendar
import datetime
import os
from copy import deepcopy

from flask import Flask, flash, render_template, request
from names import get_first_name
from pytz import all_timezones
from werkzeug.utils import redirect

from app.manager.calendar_manager import CalendarManager
from app.manager.event_manager import EventManager
from app.manager.user_manager import UserManager
from app.model.calendar import CalendarEventAccess, CalendarView
from app.model.event import EventType, RSVP, EventWriteAccess
from modules.constants import Constant
from modules.html import Html
from modules.messages import Message

app = Flask(__name__)
app.secret_key = 'some_secret'

um = UserManager()
em = EventManager()
cm = CalendarManager()

# ADMIN
um.add(name=Constant.ADMIN, time_zone="Poland", guest_event_edit_data=None)

# SAMPLE USERS
um.add(name="Kazuya", time_zone="Asia/Tokyo", guest_event_edit_data={
    'art gallery opening': [{'title': 'art gallery opening 456'}, {'description': 'art gallery opening guest desc'},
                            {'event_type': 'normal'}, {'start_date': '2016-10-13'},
                            {'end_date': '2016-10-20'}, {'start_time': '12:00'}, {'end_time': '18:00'}]})
um.add(name="Stanisław", time_zone="Poland", guest_event_edit_data=None)
um.add(name="Cassandra", time_zone="Europe/London", guest_event_edit_data=None)

# SAMPLE EVENTS
em.add(event_type=EventType.ALL_DAY.value, title="coding session 2016", description="we will write some Python code!",
       calendar="Kazuya calendar", start_date="2016-10-17", end_date="2016-10-19", start_time=None, end_time=None,
       time_zone=None, owner="Kazuya", guests={'Stanisław': 'unknown'}, owner_rsvp=RSVP.UNKNOWN.value)

em.add(event_type=EventType.NORMAL.value, title="art gallery opening", description="new art gallery will be open",
       calendar="Cassandra calendar", start_date="2016-10-15", end_date="2016-10-15", start_time="15:00",
       end_time="16:00", time_zone="Poland", owner="Cassandra", guests={'Kazuya': 'unknown', 'Stanisław': 'unknown'},
       owner_rsvp=None)

# SAMPLE CALENDARS
cm.add(name="Kazuya calendar", color="AB1871", owner="Kazuya", share={'Stanisław': 'write'})
cm.add(name="Stanisław calendar", color="39AB4F", owner="Stanisław", share=None)
cm.add(name="Cassandra calendar", color="FF88A4", owner="Cassandra", share={'Stanisław': 'write', 'Kazuya': 'read'})

os_dir_path = os.path.dirname(__file__)


@app.route('/')
def index():
    users = um.get_names(rm_names=[Constant.ADMIN])
    options = ""
    for i in users:
        options += "<option>{}</option>\n\t\t".format(i)
    return render_template("index.html", CHOOSE_USER_OPTIONS=options)


@app.route('/event_manager/rsvp')
def write_rsvp():
    user = request.args.get("user")
    events = request.args.getlist("events")
    rsvp = request.args.get("rsvp")
    for title in events:
        event = em.get(title=title)
        if event.owner == user:
            event.owner_rsvp = rsvp
        elif user in event.guests:
            event.guests[user] = rsvp
    return user_view(username=user)


@app.route('/user/view')
def user_view(username=None, error_msg=None):
    user = request.args.get('user') or username

    time_zone_options = ""
    for tz in all_timezones:
        time_zone_options += "<option value='{}'>{}</option>".format(tz, tz)

    if user == Constant.ADMIN:
        username = get_first_name()
        return render_template("view/admin_view.html", username=username, TIME_ZONE_OPTIONS=time_zone_options)
    elif user in um.get_names(rm_names=[Constant.ADMIN]):
        year_options = ""
        for i in range(2000, 2100):
            year_options += '<option value="{}">{}</option>'.format(i, i)

        owner_calendars, shared_calendars = cm.get_user_calendars(user=user)

        owner_calendar_options = ""
        for i in owner_calendars:
            owner_calendar_options += '<option value="{}">{} (owner)</option>'.format(i.name, i.name)

        calendar_options = ""
        calendar_options += owner_calendar_options
        for i in shared_calendars:
            calendar_options += '<option value="{}">{} (shared)</option>'.format(i.name, i.name)

        other_user_options = ""
        other_users = um.get_names(rm_names=[user, Constant.ADMIN])
        for i in other_users:
            other_user_options += '<option value="{}">{}</option>'.format(i, i)

        events = em.get_list()
        event_rsvp_options = ""
        for event in events:
            if user in event.guests:
                event_rsvp_options += '<option value="{}">{} (guest)</option>'.format(event.title, event.title)
            elif event.owner == user:
                event_rsvp_options += '<option value="{}">{} (owner)</option>'.format(event.title, event.title)

        if error_msg is not None:
            flash(error_msg)

        return render_template("view/user_view.html", USERNAME=user, OWNER_CALENDAR_OPTIONS=owner_calendar_options,
                               TIME_ZONE_OPTIONS=time_zone_options, OTHER_USER_OPTIONS=other_user_options,
                               CALENDAR_OPTIONS=calendar_options, YEAR_OPTIONS=year_options,
                               EVENTS_RSVP_OPTIONS=event_rsvp_options)
    else:
        return "Username '{}' don't exists.".format(user)


@app.route('/user_manager/add_user')
def add_user():
    name = request.args.get('name')
    time_zone = request.args.get('time_zone')
    if name not in um.get_names():
        um.add(name=name, time_zone=time_zone)
        return user_view(username=Constant.ADMIN)
    else:
        return "{}{}".format(user_view(username=Constant.ADMIN), Html.USER_EXISTS_MSG.format(name))


@app.route('/user_manager/delete_user')
def delete_user():
    name = request.args.get('name')
    um.delete(name=name)
    return user_list()


@app.route('/user_manager/user_list')
def user_list():
    user_list_ = um.get_list()
    user_list_view = ""
    for i in user_list_:
        user_list_view += i.name + " | " + i.time_zone + " | " + str(i.guest_event_edit_data) + \
                          Html.USER_DELETE_FORM.format(i.name)
    return render_template("list/user_list.html", user_list_view=user_list_view, user_count=len(user_list_))


@app.route('/event_manager/add_event')
def add_event():
    title = request.args.get('title')
    owner = request.args.get('event_owner')
    if title not in em.get_names():
        event_type = request.args.get('event_type')
        description = request.args.get('description')
        calendar_name = request.args.get('calendar')
        start_date = request.args.get('start_date')
        end_date = request.args.get('end_date')
        start_time = request.args.get('start_time')
        end_time = request.args.get('end_time')
        time_zone = request.args.get('time_zone')
        guests = request.args.getlist('guests')
        guest_dict = {}
        for i in guests:
            guest_dict[i] = RSVP.UNKNOWN.value
        em.add(event_type=event_type, title=title, description=description, calendar=calendar_name,
               start_date=start_date, end_date=end_date, start_time=start_time, end_time=end_time, time_zone=time_zone,
               owner=owner, guests=guest_dict, owner_rsvp=None)
    else:
        return user_view(username=owner, error_msg=Html.EVENT_EXISTS_MSG.format(title))
    return user_view(username=owner)


@app.route('/event_manager/event_list')
def event_list():
    user = request.args.get('user')
    owner_events, guest_events = em.get_user_events(user=user)
    tz_to = um.get(name=user).time_zone

    owner_events_list_view = ""
    for event in owner_events:
        event = em.adjust_event_to_user_time_zone(event=event, em=em, tz_to=tz_to)
        owner_events_list_view += event.event_type + " | " + event.title + " | " + event.description + " | " \
                                  + event.calendar + " | " + event.start_date + " | " + event.end_date + " | " + \
                                  event.owner + " | " + event.owner_rsvp + " | " + str(event.guests) \
                                  + " | " + str(event.start_time) + " | " + str(event.end_time) + " | " \
                                  + str(event.time_zone) + "</br>"

    guest_events_list_view = ""
    for event in guest_events:
        event = em.adjust_event_to_user_time_zone(event=event, em=em, tz_to=tz_to)
        guest_events_list_view += event.event_type + " | " + event.title + " | " + event.description + " | " \
                                  + event.calendar + " | " + event.start_date + " | " + event.end_date + " | " + \
                                  event.owner + " | " + event.owner_rsvp + " | " + str(event.guests) \
                                  + " | " + str(event.start_time) + " | " + str(event.end_time) + " | " \
                                  + str(event.time_zone) + "</br>"

    return render_template("list/event_list.html", owner_events_list_view=owner_events_list_view,
                           guest_events_list_view=guest_events_list_view,
                           event_count=Message.TOTAL_COUNT.format("event", str(len(owner_events + guest_events))),
                           back_url=Html.USER_BACK_VIEW.format(user))


@app.route('/calendar_manager/add_calendar')
def add_calendar():
    name = request.args.get('name')
    color = request.args.get('color')
    owner = request.args.get('owner')
    if name not in cm.get_names():
        cm.add(name=name, color=color, owner=owner, share=None)
        return user_view(username=owner)
    else:
        return user_view(username=owner, error_msg=Html.CALENDAR_EXISTS_MSG.format(name))


@app.route('/calendar_manager/calendar_list')
def calendar_list(user=None):
    user = request.args.get('user') or user
    owner_calendars, shared_calendars = cm.get_user_calendars(user=user)

    owner_calendars_list_view = ""
    for calendar_obj in owner_calendars:
        owner_calendars_list_view += calendar_obj.name + " | " + calendar_obj.color + " | " + calendar_obj.owner\
                                     + " | " + str(calendar_obj.share) + "</br>"

    shared_calendars_list_view = ""
    for calendar_obj in shared_calendars:
        shared_calendars_list_view += calendar_obj.name + " | " + calendar_obj.color + " | " + calendar_obj.owner\
                                      + " | " + str(calendar_obj.share) + "</br>"

    return render_template("list/calendar_list.html", owner_calendars_list_view=owner_calendars_list_view,
                           shared_calendars_list_view=shared_calendars_list_view,
                           calendars_count=Message.TOTAL_COUNT.format("calendars",
                                                                      len(owner_calendars + shared_calendars)),
                           back_url=Html.USER_BACK_VIEW.format(user))


@app.route('/calendar_view')
def calendar_view():
    view_type = request.args.get("type")
    user = request.args.get("user")
    calendar_name = request.args.get('calendar_name')

    calendar_obj = cm.get(calendar_name=calendar_name)
    color = calendar_obj.color
    calendar_style = open(os_dir_path + '/static/css/calendar.css').read().replace("BGCOLOR", color)
    header = ""
    html_calendar = ""

    if view_type == CalendarView.DAY.value:
        day = request.args.get('day')
        date = datetime.datetime.strptime(day, "%Y-%m-%d").date()
        week_day = calendar.day_name[date.weekday()]
        header = "Day: " + day + " (" + week_day + ")" + "</br>"
        events = em.get_events_by_date(day=day)
    elif view_type == CalendarView.WEEK.value:
        year_week = str(request.args.get('year_week'))
        end_week = datetime.datetime.strptime(year_week + '-0', "%Y-W%W-%w").date()
        start_week = str(end_week - datetime.timedelta(days=6))
        end_week = str(end_week)
        events = em.get_events_by_date(start_week=start_week, end_week=end_week)
        header = "Week: " + year_week + ": " + start_week + " - " + end_week + "</br></br>"
    elif view_type == CalendarView.MONTH.value:
        month = str(request.args.get('month'))
        y = int(month[0:4])
        m = int(month[5:7])
        html_calendar = calendar.HTMLCalendar(calendar.MONDAY).formatmonth(y, m)
        events = em.get_events_by_date(month=month)
    elif view_type == CalendarView.YEAR.value:
        year = request.args.get('year')
        html_calendar = calendar.HTMLCalendar(calendar.MONDAY).formatyear(int(str(year)))
        events = em.get_events_by_date(year=year)

    view = ""
    calendar_owner = calendar_obj.owner
    tz_to = um.get(name=user).time_zone

    user_obj = um.get(name=user)
    guest_event_edit_data = user_obj.guest_event_edit_data

    view += "<h1>Calendar view ({})</h1>".format(view_type)
    view += "<h2>Type | Title | Description | Calendar | Start date | End date | Owner | Owner RSVP |" \
            " Guests | Start time | End time | Time zone </h2>"

    no_events = True

    for event in events:
        event = em.adjust_event_to_user_time_zone(event=event, em=em, tz_to=tz_to)
        if (event.owner == user) and calendar_owner == user:
            no_events = False
            view += event.event_type + " | " + event.title + " | " + event.description + " | " \
                    + event.calendar + " | " + event.start_date + " | " + event.end_date + " | " + \
                    event.owner + " | " + event.owner_rsvp + " | " + str(event.guests) \
                    + " | " + str(event.start_time) + " | " + str(event.end_time) + " | " + str(
                    event.time_zone) + Html.EVENT_EDIT_FORM.format(EventWriteAccess.OWNER.value[0], user,
                    event.title, EventWriteAccess.OWNER.value[1]) + "</br>"
        elif (user in calendar_obj.share) and (calendar_name == event.calendar):
            no_events = False
            if calendar_obj.share[user] == CalendarEventAccess.WRITE.value:
                view += event.event_type + " | " + event.title + " | " + event.description + " | " \
                        + event.calendar + " | " + event.start_date + " | " + event.end_date + " | " + \
                        event.owner + " | " + event.owner_rsvp + " | " + str(event.guests) \
                        + " | " + str(event.start_time) + " | " + str(event.end_time) + " | " + str(
                        event.time_zone) + Html.EVENT_EDIT_FORM.format(EventWriteAccess.SHARE.value[0], user,
                        event.title, EventWriteAccess.SHARE.value[1]) + "</br>"
            elif calendar_obj.share[user] == CalendarEventAccess.READ.value:
                view += event.event_type + " | " + event.title + " | " + event.description + " | " \
                        + event.calendar + " | " + event.start_date + " | " + event.end_date + " | " + \
                        event.owner + " | " + event.owner_rsvp + " | " + str(event.guests) \
                        + " | " + str(event.start_time) + " | " + str(event.end_time) + " | " + str(
                        event.time_zone) + "</br>"
        if calendar_owner == user:
            if user in event.guests:
                no_events = False
                old_name = event.title
                if event.title in guest_event_edit_data:
                    j = deepcopy(event)
                    guest_event = guest_event_edit_data[event.title]
                    for item in guest_event:
                        for key, value in item.items():
                            setattr(j, key, value)
                    for k in user_obj.guest_event_edit_data.keys():
                        if user_obj.guest_event_edit_data[k][0]['title'] == event.title:
                            old_name = k
                    view += j.event_type + " | " + j.title + " | " + j.description + " | " \
                            + j.calendar + " | " + j.start_date + " | " + j.end_date + " | " + \
                            j.owner + " | " + j.owner_rsvp + " | " + str(j.guests) \
                            + " | " + str(j.start_time) + " | " + str(j.end_time) + " | " + str(
                            j.time_zone) + Html.EVENT_EDIT_FORM.format(EventWriteAccess.GUEST.value[0], user,
                                                                   old_name,
                                                                   EventWriteAccess.GUEST.value[1]) + "</br>"
                else:
                    view += event.event_type + " | " + event.title + " | " + event.description + " | " \
                            + event.calendar + " | " + event.start_date + " | " + event.end_date + " | " + \
                            event.owner + " | " + event.owner_rsvp + " | " + str(event.guests) \
                            + " | " + str(event.start_time) + " | " + str(event.end_time) + " | " + str(
                            event.time_zone) + Html.EVENT_EDIT_FORM.format(EventWriteAccess.GUEST.value[0], user,
                            old_name, EventWriteAccess.GUEST.value[1]) + "</br>"

    if no_events is True:
        view += "</br>" + Message.YOU_DONT_HAVE_ANY_EVENTS.format(view_type)

    style = "<style>body {background-color: #e6ffff;}</style>"
    return header + view + "</br>" + html_calendar + "</br></br>" + Html.USER_BACK_VIEW.format(user) + style + \
           calendar_style


global global_back_to_calendar_url
global_back_to_calendar_url = "/"


@app.route('/event_manager/edit')
def event_edit():
    event_name = request.args.get('name')
    access = request.args.get('access')
    user = request.args.get('user')
    event = em.get(title=event_name)
    owner = event.owner

    disabled = ""
    if access == EventWriteAccess.GUEST.value[0]:
        disabled = "disabled"

    if event.event_type == EventType.NORMAL.value:
        event_type_options = Html.EVENT_TYPE_OPTIONS.format(Constant.SELECTED, "")
    elif event.event_type == EventType.ALL_DAY.value:
        event_type_options = Html.EVENT_TYPE_OPTIONS.format("", Constant.SELECTED)

    time_zone_options = ""
    for tz in all_timezones:
        if hasattr(event, 'time_zone') and tz == event.time_zone:
            time_zone_options += "<option {} value='{}'>{}</option>".format(Constant.SELECTED, tz, tz)
        else:
            time_zone_options += "<option value='{}'>{}</option>".format(tz, tz)

    user_names = um.get_names(rm_names=[Constant.ADMIN, owner])
    choose_guests_options = ""
    for username in user_names:
        if username in event.guests:
            choose_guests_options += '<option selected>{}</option>'.format(username)
        else:
            choose_guests_options += '<option>{}</option>'.format(username)

    back_to_calendar_url = str(request.referrer)
    if "calendar_name" in back_to_calendar_url:
        global global_back_to_calendar_url
        global_back_to_calendar_url = back_to_calendar_url
        back_to_calendar_url = global_back_to_calendar_url
    elif global_back_to_calendar_url is not "/":
        back_to_calendar_url = global_back_to_calendar_url
    else:
        back_to_calendar_url = "/"

    return render_template("edit/event_edit.html",
                           OWNER_CALENDAR='<option value="{}">{} (owner)</option>'.format(event.calendar,
                                                                                          event.calendar),
                           ACCESS=access, USERNAME=user, owner_value=owner,
                           previous_event_name=event_name, event_type_options=event_type_options,
                           Event_title=event_name, Event_description=event.description, START_DATE=event.start_date,
                           START_TIME=event.start_time, END_DATE=event.end_date, END_TIME=event.end_time,
                           DISABLED_SELECT=disabled, TIME_ZONE_OPTIONS=time_zone_options,
                           CHOOSE_GUESTS_OPTIONS=choose_guests_options,
                           BACK_TO_CALENDAR_URL=back_to_calendar_url)


@app.route('/event_manager/write')
def event_write():
    access = request.args.get('access')
    previous_name = request.args.get('previous_name')
    event_type = request.args.get('event_type')
    title = request.args.get('title')
    description = request.args.get('description')
    start_date = request.args.get('start_date')
    end_date = request.args.get('end_date')
    start_time = request.args.get('start_time')
    end_time = request.args.get('end_time')
    owner = request.args.get('owner')
    new_title = title

    if previous_name != title:
        if title in em.get_names():
            return "Event title '{}' already exists !".format(title) + "</br><a href='/'>Back to index</a>"

    if access == EventWriteAccess.GUEST.value[0]:
        username = request.args.get('user')
        user = um.get(name=username)
        new_title = previous_name
        title = {"title": title}
        description = {"description": description}
        event_type = {"event_type": event_type}
        start_date = {"start_date": start_date}
        end_date = {"end_date": end_date}
        start_time = {"start_time": start_time}
        end_time = {"end_time": end_time}
        guest_event_edit_data = {previous_name: [title, description, event_type, start_date, end_date, start_time,
                                                 end_time]}
        user.guest_event_edit_data.update(guest_event_edit_data)

    elif access in [EventWriteAccess.OWNER.value[0], EventWriteAccess.SHARE.value[0]]:
        time_zone = request.args.get('time_zone')
        guests = request.args.getlist('guests')
        guest_dict = {}
        event = em.get(title=previous_name)
        for guest in guests:
            if guest not in event.guests:
                guest_dict[guest] = RSVP.UNKNOWN.value
            else:
                guest_dict[guest] = event.guests[guest]
        event.title = new_title
        event.description = description
        event.event_type = event_type
        event.start_date = start_date
        event.end_date = end_date
        event.start_time = start_time
        event.end_time = end_time
        event.time_zone = time_zone
        event.owner = owner
        event.guests = guest_dict

    x = str(request.referrer).replace("name={}".format(str(previous_name).replace(" ", "+")),
                                      "name={}".format(str(new_title).replace(" ", "+")))

    return redirect(x)


@app.route('/calendar_manager/share')
def share():
    calendars = request.args.getlist('calendars')
    users = request.args.getlist('users')
    access = request.args.get('access')
    user = request.args.get('user')
    for calendar_name in calendars:
        calendar_obj = cm.get(calendar_name=calendar_name)
        for username in users:
            calendar_obj.share[username] = access
    return user_view(username=user)


if __name__ == '__main__':
    app.run()

    # TO run on Heroku:
    # port = int(os.environ.get('PORT', 5000))
    # app.run(host='0.0.0.0', port=port)
