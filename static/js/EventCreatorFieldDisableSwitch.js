function EventCreatorFieldDisableSwitch() {
    if (document.getElementById("type").value == "all_day") {
        document.getElementById("stime").disabled = true;
        document.getElementById("etime").disabled = true;
        document.getElementById("time_zone").disabled = true;
     }
     else if (document.getElementById("type").value == "normal") {
        document.getElementById("stime").disabled = false;
        document.getElementById("etime").disabled = false;
        document.getElementById("time_zone").disabled = false;
     }
}