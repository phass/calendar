# Commentary #

- App is using Flask
- I've created model, managers to manage models, html5 views
- I'm using JavaScript to manage some actions that cannot be done by html
- I've added tests for models and all managers methods (to detect errors and changes in code)
- I've added admin user (which can't be deleted) and sample users / calendars / events
- I've assumed that user, calendar and event names must be unique
- Event date and time on user view is adjusted to his time zone
- Calendar in month / year view has color chosen at his creation (jscolor is used to pick color)
- I've added css styles, to make views more colorful
- Guest event changes are kept in user's dict (guest_event_edit_data)

###Admin can:###
- add user
- list users
- remove user

###User can:###
 - create calendar (own) / event (in own calendar)
 - list calendars (own, shared) / events (own, guest)
 - view events from own or shared calendar in calendar view (by day / week / month / year)
 - owner can edit events (event owner write access)
 - guest events appear in owner calendar (those events can be edited by guest, but only guest see those changes (event guest write access))
 - share own calendar with other user(s) with access read or write (calendar share write access)
 - send RSVP to own or guest event(s) 
 