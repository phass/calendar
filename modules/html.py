class Html:
    USER_EXISTS_MSG = "</br><font color='#cc0000'><b>Username '{}' already exists, please provide other name.</b>" \
                      "</font>"
    EVENT_EXISTS_MSG = "<font color='#cc0000'><b>Event title '{}' already exists, please provide other title." \
                       "</b>""</font>"
    CALENDAR_EXISTS_MSG = "<font color='#cc0000'><b>Calendar name '{}' already exists, please provide" \
                          " other name.</b></font>"
    USER_BACK_VIEW = "<a href='/user/view?user={}'>Back to user view</a>"
    EVENT_EDIT_FORM = "<form action='/event_manager/edit'>" \
                      "<input type='hidden' id='access' name='access' value='{}'>" \
                      "<input type='hidden' id='user' name='user' value='{}'>" \
                      "<input type='hidden' name='name' value='{}'/>" \
                      "<input type='submit' value='{}'/>" \
                      "</form>"
    EVENT_TYPE_OPTIONS = '<option {} value="normal">Normal</option>''<option {} value="all_day">All day</option>'
    USER_DELETE_FORM = "<form action='/user_manager/delete_user'><input type='hidden' name='name' value='{}'/>" \
                       "<input type='submit' value='delete'/></form>"
