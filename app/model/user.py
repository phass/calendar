class User:
    def __init__(self, name, time_zone, guest_event_edit_data=None):
        self.name = name
        self.time_zone = time_zone
        self.guest_event_edit_data = guest_event_edit_data or {}
