from enum import Enum


class CalendarView(Enum):
    DAY = "day"
    WEEK = "week"
    MONTH = "month"
    YEAR = "year"


class CalendarEventAccess(Enum):
    READ = "read"
    WRITE = "write"


class Calendar:
    def __init__(self, name, color, owner, share=None):
        self.name = name
        self.color = color
        self.owner = owner
        self.share = share or {}
