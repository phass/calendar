from enum import Enum


class EventType(Enum):
    NORMAL = "normal"
    ALL_DAY = "all_day"


class RSVP(Enum):
    UNKNOWN = "unknown"
    MAYBE = "maybe"
    YES = "yes"
    NO = "no"


class EventWriteAccess(Enum):
    GUEST = ["guest", 'Event guest write access']
    OWNER = ["owner", 'Event owner write access']
    SHARE = ["share", 'Calendar share write access']


class Event:
    def __init__(self, event_type, title, description, calendar, start_date, end_date, owner, start_time=None,
                 end_time=None, time_zone=None, guests=None, owner_rsvp=None):
        self.event_type = event_type
        self.title = title
        self.description = description or ""
        self.calendar = calendar
        self.start_date = start_date
        self.end_date = end_date
        self.owner = owner
        self.owner_rsvp = owner_rsvp or RSVP.UNKNOWN.value
        self.guests = guests or {}
        self.start_time = start_time
        self.end_time = end_time
        self.time_zone = time_zone
