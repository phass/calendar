from app.model.calendar import Calendar


class CalendarManager:
    def __init__(self, calendars=None):
        self.calendars = calendars or []

    def add(self, name, color, owner, share=None):
        assert name not in self.get_names(), "Calendar name '{}' already exists, please provide other name.".\
            format(name)
        calendar = Calendar(name=name, color=color, owner=owner, share=share)
        self.calendars.append(calendar)
        return calendar

    def get(self, calendar_name):
        calendar = next((c for c in self.get_list() if c.name == calendar_name), None)
        assert calendar is not None, "Calendar '{}' don't exists".format(calendar_name)
        return calendar

    def get_list(self):
        return self.calendars

    def get_names(self):
        return [c.name for c in self.get_list()]

    def get_user_calendars(self, user):
        calendars = self.get_list()
        owner_calendars = []
        shared_calendars = []

        for calendar in calendars:
            if calendar.owner == user:
                owner_calendars.append(calendar)
            elif user in calendar.share:
                shared_calendars.append(calendar)

        return owner_calendars, shared_calendars
