from app.model.user import User
from modules.constants import Constant


class UserManager:
    def __init__(self, users=None):
        self.users = users or []

    def get_list(self):
        return self.users

    def add(self, name, time_zone, guest_event_edit_data=None):
        assert name not in self.get_names(), "Username '{}' already exists!".format(name)
        user = User(name=name, time_zone=time_zone, guest_event_edit_data=guest_event_edit_data)
        self.users.append(user)
        return user

    def get(self, name):
        user = next((u for u in self.get_list() if u.name == name), None)
        assert user is not None, "User name '{}' don't exists".format(name)
        return user

    def delete(self, name):
        if name != Constant.ADMIN:
            user = self.get(name=name)
            self.users.remove(user)

    def get_names(self, rm_names=None):
        names = [i.name for i in self.get_list()]
        if rm_names is not None:
            for name in rm_names:
                try:
                    names.remove(name)
                except ValueError:
                    pass
        return names
