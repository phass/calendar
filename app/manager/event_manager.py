import datetime
from copy import deepcopy

from pytz import timezone

from app.model.event import Event


class EventManager:
    def __init__(self, events=None):
        self.events = events or []

    def add(self, event_type, title, description, calendar, start_date, end_date, owner, start_time=None, end_time=None,
            time_zone=None, guests=None, owner_rsvp=None):
        assert title not in self.get_names(), "Event title '{}' already exists!".format(title)
        event = Event(event_type=event_type, title=title, description=description, calendar=calendar,
                      start_date=start_date, end_date=end_date, start_time=start_time, end_time=end_time,
                      time_zone=time_zone, owner=owner, guests=guests, owner_rsvp=owner_rsvp)
        self.events.append(event)
        return event

    def get(self, title):
        event = next((e for e in self.get_list() if e.title == title), None)
        assert event is not None, "Event '{}' not exists".format(title)
        return event

    def get_list(self):
        return self.events

    def get_names(self):
        return [event.title for event in self.get_list()]

    def get_user_events(self, user):
        events = self.get_list()
        owner_events = []
        guest_events = []
        for event in events:
            if event.owner == user:
                owner_events.append(event)
            elif user in event.guests:
                guest_events.append(event)
        return owner_events, guest_events

    def get_events_by_date(self, day=None, start_week=None,
                           end_week=None, month=None, year=None):
        events = []
        event_list = self.get_list()

        if day is not None:
            for i in event_list:
                if i.start_date == day:
                    events.append(i)
        elif start_week is not None and end_week is not None:
            for i in event_list:
                if start_week <= i.start_date <= end_week:
                    events.append(i)
        elif month is not None:
            for i in event_list:
                if i.start_date.startswith(month):
                    events.append(i)
        elif year is not None:
            for i in event_list:
                if i.start_date.startswith(year):
                    events.append(i)
        return events

    @staticmethod
    def adjust_event_to_user_time_zone(event, em, tz_to):
        if hasattr(event, 'time_zone'):
            event_copy = deepcopy(event)
            tz_from = event.time_zone
            if tz_from != tz_to:
                event_copy.start_date, event_copy.start_time, event_copy.end_date, event_copy.end_time =\
                    em.convert_event_date_and_time_to_user_time_zone(event.start_date, event.start_time,
                                                                     event.end_date, event.end_time, tz_from, tz_to)
                return event_copy
        return event

    @staticmethod
    def convert_event_date_and_time_to_user_time_zone(start_date, start_time, end_date, end_time, tz_from, tz_to):

        if tz_from is None or tz_to is None or start_time is None or end_time is None:
            return start_date, start_time, end_date, end_time

        tz_from = timezone(tz_from)
        start = datetime.datetime(year=int(start_date[0:4]), month=int(start_date[5:7]), day=int(start_date[8:10]),
                                  hour=int(start_time[0:2]), minute=int(start_time[3:6]))
        start = tz_from.localize(start, is_dst=None)
        end = datetime.datetime(year=int(end_date[0:4]), month=int(end_date[5:7]), day=int(end_date[8:10]),
                                hour=int(end_time[0:2]), minute=int(end_time[3:6]))
        end = tz_from.localize(end, is_dst=None)

        tz_to = timezone(tz_to)
        start_other = start.astimezone(tz=tz_to)
        end_other = end.astimezone(tz=tz_to)
        start_date = start_other.strftime("%Y-%m-%d")
        start_time = start_other.strftime("%H:%M")
        end_date = end_other.strftime("%Y-%m-%d")
        end_time = end_other.strftime("%H:%M")

        return start_date, start_time, end_date, end_time
