# README #

### App url
http://pycalendar.herokuapp.com/

###TODO
- small refactoring
- use render_templete() to create some views

### What is this repository for? ###

* Calendar for many users
* v 1.1

### How do I get set up? ###

* Install PyCharm
* Install requirements.txt
* Run tests with py.test
* Run app.py
* **Choice of date and time works best on Chrome (otherwise sometimes must be typed manualy)**

### Who do I talk to? ###

* Repo owner